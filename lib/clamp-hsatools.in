#!/bin/bash
# Compiles an LLVM bitcode file to HSAIL

# enable bash debugging
KMDBSCRIPT="${KMDBSCRIPT:=0}"

# dump the LLVM bitcode
KMDUMPLLVM="${KMDUMPLLVM:=0}"

# dump the brig
KMDUMPBRIG="${KMDUMPBRIG:=0}"

# pass extra options to llc
KMLLOPT="${KMLLOPT:=""}"

if [ $KMDBSCRIPT == "1" ]; then
  set -x
fi

# check command line argument
if [ "$#" -ne 1 ]; then
  echo "Usage: $0 input_LLVM_IR" >&2
  exit 1
fi

if [ ! -f $1 ]; then
  echo "input LLVM IR $1 is not valid" >&2
  exit 1
fi

# tools search priority:
# 1) $HCC_HOME
# 2) @CMAKE_INSTALL_PREFIX@ : default install directory
# 3) @PROJECT_BINARY_DIR@ : build directory

if [ -n "$HCC_HOME" ] && [ -e "$HCC_HOME" ]; then
    HLC_LLVM_LINK=$HCC_HOME/hlc/bin/llvm-link
    HLC_OPT=$HCC_HOME/hlc/bin/opt
    HLC_LLC=$HCC_HOME/hlc/bin/llc
    HLC_ASM=$HCC_HOME/HSAILasm/HSAILasm
    NEWLIB=$HCC_HOME/lib
elif [ -e @CMAKE_INSTALL_PREFIX@ ]; then
    HLC_LLVM_LINK=@CMAKE_INSTALL_PREFIX@/hlc/bin/llvm-link
    HLC_OPT=@CMAKE_INSTALL_PREFIX@/hlc/bin/opt
    HLC_LLC=@CMAKE_INSTALL_PREFIX@/hlc/bin/llc
    HLC_ASM=@CMAKE_INSTALL_PREFIX@/HSAILasm/HSAILasm
    NEWLIB=@CMAKE_INSTALL_PREFIX@/lib
elif [ -d @PROJECT_BINARY_DIR@ ]; then
    HLC_LLVM_LINK=@PROJECT_BINARY_DIR@/hlc/bin/llvm-link
    HLC_OPT=@PROJECT_BINARY_DIR@/hlc/bin/opt
    HLC_LLC=@PROJECT_BINARY_DIR@/hlc/bin/llc
    HLC_ASM=@PROJECT_BINARY_DIR@/HSAILasm/HSAILAsm/HSAILasm # notice the "A"
    NEWLIB=@CPPAMP_SOURCE_DIR@/lib
else
    echo "ERROR: Can NOT locate HCC tools! Please specify with $HCC_HOME environmental variable." >&2
    exit 1
fi

if [ -n "@HSA_LLVM_BIN_DIR@" ]; then
    HLC_LLVM_LINK=@HSA_LLVM_BIN_DIR@/llvm-link
    HLC_OPT=@HSA_LLVM_BIN_DIR@/opt
    HLC_LLC=@HSA_LLVM_BIN_DIR@/llc
fi

HSA_USE_AMDGPU_BACKEND=@HSA_USE_AMDGPU_BACKEND@

if [ $HSA_USE_AMDGPU_BACKEND == "ON" ]; then
  LLD=@HSA_LLVM_BIN_DIR@/lld
  KM_USE_AMDGPU="${KM_USE_AMDGPU:=1}"
fi

if [ $KMDUMPLLVM == "1" ]; then
  cp $1 ./dump.fe.bc
fi

if [ $KM_USE_AMDGPU ]; then
  EXTRA_LIBRARY=@HSA_AMDGPU_BUILTIN_LIBRARY_WRAPPER@
fi

$HLC_LLVM_LINK -suppress-warnings -o $1.linked.bc $1 $NEWLIB/builtins-hsail.opt.bc $EXTRA_LIBRARY

# error handling for HSAIL llvm-link
RETVAL=$?
if [ $RETVAL != 0 ]; then
  exit $RETVAL
fi

if [ $KMDUMPLLVM == "1" ]; then
  cp $1.linked.bc ./dump.linked.bc
fi


# Optimization notes:
#  -disable-simplify-libcalls:  prevents transforming loops into library calls such as memset, memcopy on GPU 
$HLC_OPT -O3 -disable-simplify-libcalls -verify $1.linked.bc -o $1.opt.bc
# error handling for HSAIL opt
RETVAL=$?
if [ $RETVAL != 0 ]; then
  exit $RETVAL
fi

if [ $KMDUMPLLVM == "1" ]; then
  cp $1.opt.bc ./dump.opt.bc
fi

if [ $KM_USE_AMDGPU  ]; then
  $HLC_LLC -O2 -mtriple amdgcn--amdhsa -mcpu=kaveri -filetype=obj -o $1.hsail $1.opt.bc
else
  $HLC_LLC -O2 -march=hsail64 -filetype=asm -o $1.hsail $1.opt.bc
fi

# error handling for HSAIL llc
RETVAL=$?
if [ $RETVAL != 0 ]; then
  exit $RETVAL
fi

if [ $KMDUMPBRIG == "1" ]; then
    cp $1.hsail ./dump.hsail
fi

if [ $KM_USE_AMDGPU ]; then
  $LLD -flavor gnu -target amdgcn--amdhsa $1.hsail -o $1.brig
else
  cat $NEWLIB/hsa_builtins.hsail >> $1.hsail
  $HLC_ASM -assemble -o $1.brig $1.hsail
fi

# error handling for HSAILasm
RETVAL=$?
if [ $RETVAL != 0 ]; then
  exit $RETVAL
fi

if [ $KMDUMPBRIG == "1" ]; then
    cp $1.brig ./dump.hsa_builtins.brig
fi
